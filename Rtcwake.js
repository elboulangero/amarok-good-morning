/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-morning amarok script.                       */
/*                                                                            */
/* good-morning is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-morning is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-morning.  If not, see <http://www.gnu.org/licenses/>.      */
/******************************************************************************/

function Rtcwake()
{
    Amarok.debug("-> " + arguments.callee.name);

    this.path = "";
    this.seconds = -1;
    this.enabled = false;

    Amarok.debug("<- " + arguments.callee.name);
}

Rtcwake.prototype.set = function(path, seconds)
{
    Amarok.debug("-> " + "Rtcwake." + "set");

    this.path = path;
    this.seconds = seconds;

    if (doProcess(path + " " + seconds) == 0) {
	this.enabled = true;
	Amarok.debug("RTC wake scheduled in " + seconds + " seconds");
	Amarok.debug("That's to say: " + durationToString(seconds));
    } else {
	Amarok.debug("Failed to setup rtcwake !");
	Amarok.alert("Failed to setup rtcwake !");
    }

    Amarok.debug("<- " + "Rtcwake." + "set");
}

Rtcwake.prototype.unset = function()
{
    Amarok.debug("-> " + "Rtcwake." + "unset");

    if (this.enabled == true) {
	if (doProcess(this.path + " " + 0) == 0) {
	    Amarok.debug("RTC wake disabled");
	} else {
	    Amarok.debug("Failed to disable rtcwake !");
	    Amarok.alert("Failed to disable rtcwake !");
	}
	this.enabled = false;
    }

    Amarok.debug("<- " + "Rtcwake." + "unset");
}

// Private functions

function durationToString(seconds)
{
    var time = new Date(seconds * 1000);
    var days = time.getUTCDate() - 1;
    var hours = time.getUTCHours();
    var minutes = time.getUTCMinutes();
    var seconds = time.getUTCSeconds();

    return (days ? days + " days, " : "") +
	(hours ? hours + " hours, " : "") +
	(minutes ? minutes + " minutes, " : "") +
	(seconds ? seconds + " seconds" : "");
}

function doProcess(cmd)
{
    var process = new QProcess();
    process.start(cmd);
    if (!process.waitForFinished(-1)) {
	Amarok.debug(cmd + ": failed to execute.");
	return -1;
    }
    if (process.exitCode() != 0) {
	Amarok.debug(cmd + ": returned " + process.exitCode());
	return -1;
    }

    return 0;
}