/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-morning amarok script.                       */
/*                                                                            */
/* good-morning is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-morning is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-morning.  If not, see <http://www.gnu.org/licenses/>.      */
/******************************************************************************/

Importer.include("Fadein.js");
Importer.include("Rtcwake.js");

function Alarm(gui)
{
    Amarok.debug("-> " + arguments.callee.name);

    // Gui
    this.gui = gui;

    // Receiver
    this.receiver = null;
    this.member = null;

    // Alarm date
    this.alarmDate = null;

    // Countdown to alarm
    this.countdown = new QTimer();
    this.countdown.timeout.connect(this, this.countdownTimeout);

    // Fade-in
    this.fader = new Fadein();

    // RTC
    this.rtcwake = new Rtcwake();

    Amarok.debug("<- " + arguments.callee.name);
}

Alarm.prototype.connect = function(receiver, member)
{
    Amarok.debug("-> Alarm." + "connect");

    this.receiver = receiver;
    this.member = member;

    Amarok.debug("<- Alarm." + "connect");
}

Alarm.prototype.start = function(config)
{
    Amarok.debug("-> Alarm." + "start");

    // Stop and init some values
    this.countdown.stop();
    this.alarmDate = null;

    // Now
    var now_date = new Date();
    var now_day = now_date.getDay();
    var now_day_index = (now_day + 6) % 7;

    // Find if an exception is scheduled
    if (config.exception.enabled == true) {
	Amarok.debug("Alarm scheduled is an exception");
	if (config.exception.timestamp > now_date) {
	    this.alarmDate = new Date(config.exception.timestamp);
	} else {
	    // Shouldn't happen
	     Amarok.debug("Exception already passed !");
	}
    }

    // Otherwise, browse through the list of days to find the next alarm
    if (this.alarmDate == null) {
	Amarok.debug("Alarm scheduled as usual");
	// Loop must go until 8 to handle the unlikely case
	// where someone wakes up 1 day per week, and
	// the time of this alarm is already past.
	for (var i = 0; i < 8; i++) {
	    var day_index = (now_day_index + i) % 7;
	    if (config.usual[day_index].enabled == true) {
		var alarm_date = new Date(now_date.getTime());
		alarm_date.setDate(alarm_date.getDate() + i);
		alarm_date.setHours(config.usual[day_index].time.hour());
		alarm_date.setMinutes(config.usual[day_index].time.minute());
		alarm_date.setSeconds(0);
		alarm_date.setMilliseconds(0);
		if (alarm_date > now_date) {
		    this.alarmDate = new Date(alarm_date.getTime());
		    break;
 		} else {
		    // Alarm for this day is already passed.
		    // This can happen only for i = 0
		    Amarok.debug("Alarm already passed for day " + i);
		}
	    }
	}
    }

    // If no alarm is scheduled, return
    if (this.alarmDate == null) {
	Amarok.debug("No alarm scheduled")
	return;
    }

    // Setup rtcwake one minute before alarm
    // We use rtcwake with the --seconds option, so that days when
    // daylight saving changes are managed automagically.
    if (config.rtc.enabled == true) {
	var msecs_to_alarm = this.alarmDate.getTime() - now_date.getTime();
	var secs_to_alarm = Math.floor(msecs_to_alarm / 1000);
	var secs_to_rtcwake = secs_to_alarm - 60;

	// Ask for at least two minutes before alarm.
	// Only useful for people testing the script, they may
	// try to rtcwake in one minute to see if it works.
	if (secs_to_rtcwake < 60) {
	    Amarok.debug("RTC wake disabled, it needs more time before next alarm");
	    Amarok.alert("RTC wake disabled, it needs more time before next alarm.");
	} else {
	    this.rtcwake.set(config.rtc.scriptpath, secs_to_rtcwake);
	}
    }

    // Start countdown to alarm
    this.countdown.start(0);

    Amarok.debug("Alarm scheduled at " + this.alarmDate);
    Amarok.debug("<- Alarm." + "start");
}

Alarm.prototype.stop = function()
{
    Amarok.debug("-> Alarm." + "stop");

    // Fader
    this.fader.stop();

    // Countdown
    this.countdown.stop();

    // Rtcwake
    this.rtcwake.unset();

    Amarok.debug("<- Alarm." + "stop");
}

Alarm.prototype.action = function(config)
{
    Amarok.debug("-> Alarm." + "action");

    // Log time for debug
    Amarok.debug("Alarm started at: " + new Date());

    // Get a few Amarok values
    var engine_state = Amarok.Engine.engineState();
    var track_index = Amarok.Playlist.activeIndex();
    var track_count =  Amarok.Playlist.totalTrackCount();

    // Do nothing if Amarok is already playing
    if (engine_state == 0) {
	Amarok.debug("Already playing");
	return;
    }

    // Set track
    if (config.music.track.enabled == true) {
	var index = config.music.track.index;
	switch (index) {
	case 0: // First
	default:
	    track_index = 0;
	    break;
	case 1: // Next
	    track_index = (track_index + 1) % track_count;
	    break;
	case 2: // Last
	    track_index = track_count - 1;
	    break;
	}
	Amarok.debug("Track set to " + track_index);
    }

    // Set progression
    if (config.music.progression.enabled == true) {
	var index = config.music.progression.index;
	Amarok.Engine.repeatPlaylist = false;
	Amarok.Engine.repeatTrack = false;
	Amarok.Engine.randomMode = false;
	switch (index) {
	default:
	case 0: // Standard
	    break;
	case 1: // Repeat Track
	    Amarok.Engine.repeatTrack = true;
	    break;
	case 2: // Repeat Playlist
	    Amarok.Engine.repeatPlaylist = true;
	    break;
	case 3: // Random Tracks
	    Amarok.Engine.randomMode = true;
	    break;
	}
	Amarok.debug("Progression set to " + index);
    }

    // Set volume
    if (config.sound.volume.enabled == true) {
	var volume = config.sound.volume.value;
	Amarok.Engine.volume = volume;
	Amarok.debug("Volume set to " + volume);
    }

    // Start fade-in
    if (config.sound.fadein.enabled == true) {
	var volume = Amarok.Engine.volume;
	var length = config.sound.fadein.value * 60;
	this.fader.start(length, volume);
	Amarok.debug("Fade-in started for " + length + " seconds");
    }

    // Play
    Amarok.Playlist.playByIndex(track_index);

    Amarok.debug("<- Alarm." + "action");
}

Alarm.prototype.countdownTimeout = function()
{
//    Amarok.debug("-> " + "Alarm." + "countdownTimeout");

    var time_left = this.alarmDate.getTime() - new Date().getTime();

    // Update gui status
    var text = "";
    var one_day = 1000 * 60 * 60 * 24;
    var one_hour = 1000 * 60 * 60;
    var one_minute = 1000 * 60;
    if (time_left > one_day) {
	var days_left = Math.ceil(time_left / one_day);
	text = days_left + " " + (days_left == 1 ? "day" : "days");
    } else if (time_left > one_hour) {
	var hours_left = Math.ceil(time_left / one_hour);
	text = hours_left + " " + (hours_left == 1 ? "hour" : "hours");
    } else {
	var minutes_left = Math.ceil(time_left / one_minute);
	text = minutes_left + " " + (minutes_left == 1 ? "minute" : "minutes");
    }

    this.gui.setStatus("Enabled ! Less than " + text + " left...");

    // Check time left
    if (time_left <= 0) {
	this.countdown.stop();
	this.member.call(this.receiver);
	return;
    }
    if (time_left <= 60 * 1000) {
	this.countdown.interval = time_left;
    } else {
	this.countdown.interval = 60 * 1000;
    }

//    Amarok.debug("<- " + "Alarm." + "countdownTimeout");
}
