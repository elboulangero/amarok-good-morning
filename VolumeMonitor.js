/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-morning amarok script.                       */
/*                                                                            */
/* good-morning is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-morning is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-morning.  If not, see <http://www.gnu.org/licenses/>.      */
/******************************************************************************/

function VolumeMonitor()
{
    Amarok.debug("-> " + arguments.callee.name);

    // Receiver
    this.receiver = null;
    this.member = null;

    // Mute for the next <limit> incoming volumeChanged signals
    this.mute = false;
    this.limit = 0;
    this.count = 0;

    Amarok.debug("<- " + arguments.callee.name);
}

VolumeMonitor.prototype.connect = function(receiver, member)
{
    Amarok.debug("-> VolumeMonitor." + "connect");

    // Save references
    this.receiver = receiver;
    this.member = member;

    // Start monitoring volume
    this.mute = false;
    Amarok.Engine.volumeChanged.connect(this, this.changed);

    Amarok.debug("<- VolumeMonitor." + "connect");
}

VolumeMonitor.prototype.disconnect = function()
{
    Amarok.debug("-> VolumeMonitor." + "disconnect");

    if (this.receiver == null) {
	return;
    }

    // Reset references
    this.receiver = null;
    this.member = null;

    // Stop monitoring volume
    Amarok.Engine.volumeChanged.disconnect(this, this.changed);

    Amarok.debug("<- VolumeMonitor." + "connect");
}

VolumeMonitor.prototype.muteFor = function(value)
{
//    Amarok.debug("-> VolumeMonitor." + "muteFor");

    this.mute = true;
    this.limit = value;
    this.count = 0;

//    Amarok.debug("<- VolumeMonitor." + "muteFor");
}

// Private methods

VolumeMonitor.prototype.changed = function()
{
//    Amarok.debug("-> VolumeMonitor." + "changed");

    if (this.mute == false) {
	Amarok.debug("Volume changed by user !");
	this.member.call(this.receiver);
    } else {
	this.count++;
//	Amarok.debug("count: " + this.count);
	if (this.count == this.limit) {
	    this.mute = false;
	}
    }

//    Amarok.debug("<- VolumeMonitor." + "changed");
}
