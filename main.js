/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-morning amarok script.                       */
/*                                                                            */
/* good-morning is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-morning is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-morning.  If not, see <http://www.gnu.org/licenses/>.      */
/******************************************************************************/

Importer.loadQtBinding("qt.core");
Importer.loadQtBinding("qt.gui");
Importer.loadQtBinding("qt.uitools");

Importer.include("ConfigIO.js");
Importer.include("Gui.js");
Importer.include("Alarm.js");

// Default config, passed by reference to every object.
// It should be modified only by:
//  + ConfigIO.read()
//  + Gui.getValues()
//
// We store time with QTime object:
//  + it's pretty convenient to store time only, and to get/set gui values.
//  + it's unusable in JS, since apparently, most of the functions
//    (like dayOfWeek(), addDays(), MSecsSinceEpoch() and more) are
//    missing in the binding.
//    So, when it comes to work with dates, we use JS Date instead.
//
// Exception is automatically disabled when it's past.
// As a consequence, storing the hour only is not enough, we need a whole date.
var config = {
    "enabled" : false,
    "usual": [
        { //monday
	    "enabled": true,
	    "time": QTime.fromString("08:00:00")
        },
	{ // tuesday
            "enabled": true,
            "time": QTime.fromString("07:00:00")
        },
	{ // wednesday
            "enabled": true,
            "time": QTime.fromString("07:00:00")
        },
	{ // thursday
            "enabled": true,
            "time": QTime.fromString("07:00:00")
        }, // friday
	{
            "enabled": true,
            "time": QTime.fromString("07:00:00")
        }, // saturday
	{
            "enabled": false,
            "time": QTime.fromString("10:00:00")
        }, //sunday
	{
            "enabled": false,
            "time": QTime.fromString("10:00:00")
        }
    ],
    "exception": {
        "enabled": false,
	"timestamp": 0
    },
    "music": {
        "track": {
	    "enabled": false,
	    "index": 0
        },
        "progression": {
	    "enabled": false,
	    "index": 0
        }
    },
    "sound": {
        "volume": {
            "enabled": false,
            "value": 100
        },
        "fadein": {
            "enabled": false,
            "value": 5
        }
    },
    "rtc": {
	"enabled": false,
	"scriptpath": Amarok.Info.scriptPath() + "/rtc-scripts/rtcwake-sudo.sh"
    }
};

// Get config
ConfigIO.read(config);

// Create the GUI, set values from conf
var gui = new Gui();
gui.dialog.actionGroupBox.disableButton.clicked.connect(disable);
gui.dialog.actionGroupBox.enableButton.clicked.connect(enable);
gui.setValues(config);

// Create the alarm, start if enabled
var alarm = new Alarm(gui);
alarm.connect(this, action);
if (config.enabled == true) {
    alarm.start(config);
}

// Amarok tools menu
Amarok.Window.addToolsMenu("GoodMorning", "Good Morning", Amarok.Info.scriptPath() + "/icons/sunny.png");
Amarok.Window.ToolsMenu.GoodMorning["triggered()"].connect(Amarok.Window.ToolsMenu.GoodMorning, function() { gui.show(config.enabled); });



function disable()
{
    Amarok.debug("-> " + arguments.callee.name);

    // Update conf
    gui.getValues(config);
    config.enabled = false;

    // Stop alarm
    alarm.stop();

    // Write conf
    ConfigIO.write(config);

    Amarok.debug("<- " + arguments.callee.name);
}

function enable()
{
    Amarok.debug("-> " + arguments.callee.name);

    // Update conf
    gui.getValues(config);
    config.enabled = true;

    // Start alarm
    alarm.start(config);

    // Write conf
    ConfigIO.write(config);

    Amarok.debug("<- " + arguments.callee.name);
}

function action()
{
    Amarok.debug("-> " + arguments.callee.name);

    // Invoke alarm action
    alarm.action(config);

    // Disable exception
    if (config.exception.enabled == true) {
	config.exception.enabled = false;
	gui.setValues(config);
    }

    // Start next alarm
    alarm.start(config);

    Amarok.debug("<- " + arguments.callee.name);
}
